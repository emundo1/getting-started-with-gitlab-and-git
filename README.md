# Getting Started with GitLab and Git
- [Installing and configuring Git](https://its-git.ga.com/integrity-life-cycle-manager-team/resources/getting-started-with-gitlab-and-git/blob/master/README.md#installing-and-configuring-git)
- [Getting started with Git and GitLab](https://its-git.ga.com/integrity-life-cycle-manager-team/resources/getting-started-with-gitlab-and-git/blob/master/README.md#getting-started-with-git-and-gitlab)
- [Getting familiar with our workflow](https://its-git.ga.com/integrity-life-cycle-manager-team/resources/getting-started-with-gitlab-and-git/blob/master/README.md#getting-familiar-with-our-workflow)
- [Continue learning about Git and GitLab](https://its-git.ga.com/integrity-life-cycle-manager-team/resources/getting-started-with-gitlab-and-git/blob/master/README.md#continue-learning-about-git-and-gitlab)

# Installing and configuring Git.
##### Installing Git
&nbsp;&nbsp;&nbsp;&nbsp;You can go to the following website to install `git`. You will need administrative rights on your computer to install this software. 
- [Git Install](https://git-scm.com/download/win)

# Getting started with Git and GitLab
##### Git basics
&nbsp;&nbsp;&nbsp;&nbsp;To begin, you should read up on how to use the `git` command-line tool. This will be the primary and most straightforward way of putting code from your local machine onto the `GitLab` repository. The recommended resource for learning `git` can be found at the following link:

- [Start Using Git Doc](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

##### Command line functions
&nbsp;&nbsp;&nbsp;&nbsp;After that, it's recommended but not required to learn some basic command line functions for navigating using the command line. This will give you the basics concepts for manipulating files using the command line. The following link is the recommended resource:

- [Command Line Basics Doc](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html)


###### Basics of GitLab
&nbsp;&nbsp;&nbsp;&nbsp;Finlay, you should become comfortable with the following topics related to `GitLab`. Bellow will be a list of topics with accompanying resources.

- **[Create a project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)**
- **[Create a new issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-a-new-issue)**
- **[How to create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)**
- **[Feature branch workflow](https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html)**

# Getting familiar with our workflow
##### GitLab Flow
&nbsp;&nbsp;&nbsp;&nbsp;After you feel comfortable using `Git` and `GitLab`, you should review the *GitLab flow workflow*. This workflow is the standard that we will be using. This workflow should be followed when working with projects in our repository. The recommended resource for this topic can be found at the following link:

- https://docs.gitlab.com/ee/topics/gitlab_flow.html#squashing-commits-with-rebase

# Continue learning about Git and GitLab
##### GitLab documentation
&nbsp;&nbsp;&nbsp;&nbsp;The resources above are a primer and should be what you need to get started using `Git` and `GitLab` but is not an all-encompassing guide. Many tools are not covered above. If you wish to continue learning about `Git` and `GitLab`, you should read the full documentation found of docs.gitlab.com. A link to that resource can be found below:

- https://docs.gitlab.com/ee/README.htm